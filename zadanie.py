import numpy
import pandas
import os
from random import randint
from matplotlib import pyplot
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error, r2_score


class NumpyArray:

    def __init__(self, size):
        """
        Create array and define min and max values
        """
        numpy_array = self._create_numpy_array(size)
        self.numpy_array = numpy_array
        self.min_value = min(numpy_array)
        self.max_value = max(numpy_array)

    def _create_numpy_array(self, size):
        """
        Create numpy array
        """
        array = numpy.array([0])
        for i in range(1, size):
            array = numpy.append(array, array[i-1] + pow(-1, randint(1,2)))
        return array

    def chart(self, kind, **kwargs):
        """
        Show chart, use object's array as values if kwargs are empty
        """
        pyplot.cla()
        if kwargs:
            getattr(pyplot, kind)(**kwargs)
        else:
            getattr(pyplot, kind)( self.numpy_array.tolist())
        pyplot.show()
        pyplot.clf()

    def simulation(self, count, size, value):
        """
        Simulate how many times exceed value x in y numpy arrays
        """
        arrays = [self._create_numpy_array(size) for i in range(count)]
        result = 0
        for array in arrays:
            res = 0
            for val in numpy.ndenumerate(array):
                res += val[1]
                if res > value:
                    result += 1
                    break
        data = {
            'height': [result, count - result],
            'x': ['> {}'.format(value), '< {}'.format(value)]
        }
        self.chart('bar', **data)


class PandasDataFrame:

    def __init__(self, datas):
        """
        Create data frame from cvs if path is in kwargs
        """
        if 'filepath_or_buffer' in datas:
            self.frame = pandas.read_csv(**datas)
        else:
            self.frame = pandas.DataFrame(**datas)

    def correct_frame(self):
        """
        Correct some values in frame
        """
        for index in self.frame.index.values:
            self.frame.rename(index={index: index.capitalize()}, inplace=True)
        return self.frame

    def search(self, query):
        """
        Filter frame and return result
        """
        return self.frame[query]

    def add(self, rows, index):
        """
        Append rows to data frame
        """
        frame = pandas.DataFrame(rows, columns=list(self.frame), index=index)
        self.frame = self.frame.append(frame)
        return self.frame

    def sort(self, value, ascending=True):
        """
        Sort frame by value ascending or descending
        """
        return self.frame.sort_values(by=value, ascending=ascending)

    def move_column(self, column, position):
        """
        Move column
        """
        headers = list(self.frame)
        headers[headers.index(column)], headers[position] = headers[position], headers[headers.index(column)]
        self.frame = self.frame[headers]
        return self.frame

    def remove(self, index):
        """
        Remove row from frame
        """
        self.frame = self.frame.drop(index=index)
        return self.frame


def task_3():
    """
    Task 3
    """
    # A, B
    array = NumpyArray(100)
    print('Numpy array with 0 as first element and filled with 100 numbers')
    print(array.numpy_array)
    print('\n')

    # C
    print('Chart')
    array.chart('plot')
    print('\n')

    # D
    print('Min value of array: {}'.format(array.min_value))
    print('Max value of array: {}'.format(array.max_value))
    print('\n')

    # E
    print('Simulation of creating 50 numpy arrays and check if sum of numbers in any is more than 30')
    array.simulation(50, 100, 30)
    print('\n')


def task_4():
    """
    Task 4
    """
    data = {
        'filepath_or_buffer': '{}/pandas.csv'.format(os.path.dirname(os.path.realpath(__file__))),
        'sep': ';',
        'index_col': 0,
        'converters': {'powierzchnia': float, 'ludność': float},
        'skiprows': 0
    }
    data = PandasDataFrame(data)

    # A
    print('Provinces with area less than 20000')
    print(data.search((data.frame['powierzchnia'] < 20000)))
    print('\n')

    # B
    print('Provinces with population more than 2000000 and save result as new object')
    data_2 = data.search((data.frame['ludność'] > 2000000))
    print(data_2)
    print('\n')

    # C
    print('Add new province')
    print(data.add([[30, 'Poznań', 29826.50, 3475323]], ['wielkopolska']))
    print('\n')

    # D
    print('Order provinces by population descending')
    print(data.sort('ludność', False))
    print('\n')

    # E
    print('Move column to the end')
    print(data.move_column('miasto wojewódzkie', -1))
    print('\n')

    # F
    print('Move column to the end')
    print(data.correct_frame())
    print('\n')

    # G, I
    print('Create series with provinces as indexes and bool value if population/km2 is more than 140')
    series = pandas.Series(
        [True if row['ludność'] / row['powierzchnia'] > 140 else False for i, row in data.frame.iterrows()],
        index=data.frame.index.values
    )
    print(series)
    print(series.describe())
    print('\n')

    # H
    print('Remove row')
    print(data.remove('Lubuskie'))
    print('\n')


class MachineLearning:

    def __init__(self, datas, target, column_weight):
        """
        Create data frame
        """
        if 'filepath_or_buffer' in datas:
            frame = pandas.read_csv(**datas)
        else:
            frame = pandas.DataFrame(**datas)
        self._model = LinearRegression()
        self.target = frame[target]
        self.formula_frame = self._count_formula(frame, target, column_weight)
        del frame

    def _count_formula(self, frame, target, column_weight):
        """
        Create new data frame which is result of counted formula
        """
        data = {'formula': []}

        for i, row in frame.loc[:, frame.columns != target].iterrows():
            result = 0
            for column in row.index:
                result += row[column] * (column_weight[column] if column in column_weight else 0)
            data['formula'].append(result)

        return pandas.DataFrame(data=data)

    def train(self, size=0.3):
        """
        Train machine
        """
        feature_train, feature_test, prediction_train, prediction_test = \
        train_test_split(
            self.formula_frame,
            self.target,
            test_size=size
        )

        x_test = pandas.DataFrame(data=feature_test)
        y_test = pandas.DataFrame(data=prediction_test)

        self._model.fit(feature_train, prediction_train)

        return x_test, y_test

    def predict(self, test_data):
        """
        Predict
        """
        return self._model.predict(test_data)


def task_5():
    """
    Task 5
    """
    # B
    data = {
        'filepath_or_buffer': '{}/winequality-red.csv'.format(os.path.dirname(os.path.realpath(__file__))),
        'sep': ',',
        'index_col': False,
        'converters': {
            'fixed acidity': float,
            'volatile acidity': float,
            'citric acid': float,
            'residual sugar': float,
            'chlorides': float,
            'free sulfur dioxide': float,
            'total sulfur dioxide': float,
            'density': float,
            'pH': float,
            'sulphates': float,
            'alcohol': float,
            'quality': float
        }
    }

    column_weight = {
        'alcohol': 1000,
        'volatile acidity': -1000,
        'ph': -250
    }

    learning = MachineLearning(data, 'quality', column_weight)
    x_test, y_test = learning.train()
    prediction = learning.predict(x_test)

    print('Coefficients: {}'.format(learning._model.coef_))
    print('Mean squared error: {}'.format(mean_squared_error(y_test, prediction)))
    print('Variance: {}'.format(r2_score(y_test, prediction)))

    pyplot.cla()
    pyplot.scatter(x_test, y_test, color='red')
    pyplot.plot(x_test, prediction, color='blue')
    pyplot.title('Wine quality prediction')
    pyplot.show()


if __name__ == '__main__':
    """
    Tasks for recruitment process
    """
    task_3()
    task_4()
    task_5()
